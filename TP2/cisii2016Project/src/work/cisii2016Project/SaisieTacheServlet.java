package cisii2016Project;

import java.io.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SaisieTacheServlet  extends HttpServlet {

	 public  void doGet(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException  {
	
		response.setContentType("text/html") ;
		
		PrintWriter out = response.getWriter() ;

		out.println("<html>") ;
		out.println("<head>") ;
		out.println("<style>body {background-color:lightgray} h1  {color:blue} p{color:green}</style>");
		out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO_8859-1\">") ;
		
		out.println("<title>Saisie Tâche</title>") ;
		out.println("</head>") ;
		out.println("<body>") ;
		out.println("<h1 color=\"red\">Formulaire de création de tâche</h1>") ;
		out.println("<form action=\"/form/list\"  method=\"post\">") ;
		out.println("<p>Nom Tache: <input type=\"text\" name=\"nom\" /></p>") ;
		out.println(" <p>Estimation : <input type=\"text\" name=\"estim\" /></p>") ;
		out.println(" <p>Reste à faire : <input type=\"text\" name=\"raf\" /></p>") ;
		out.println("<p><input type=\"submit\"  value=\"valider\" /></p>") ;
		out.println("</form>") ;
		out.println("</body>") ;
		out.println("</html>") ;
		System.out.println("FIN");
	
	}

	 public  void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException  {
	
		doGet(request, response) ;
	}
}	