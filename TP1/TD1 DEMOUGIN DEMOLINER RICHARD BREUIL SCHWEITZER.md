###TD1 DEMOUGIN DE MOLINER BREUIL SCHWEITZER RICHARD

##Installation d'eclipse
- Telecharger l'installeur et sélectionner "IDE Java" pour lancer l'installation 

##Installation d'un serveur GitStack
- téléchargement du logiciel
- mettre le numéro de port souhaité
- aller sur localhost:[numero de port]/gitstack
- créer un repository et des users, leurs donner les accès

##Installation du plugin Egit
- Eclipse -> Help -> Eclipse MarketPlace -> chercher "Egit" -> Installer 

##Utilisation
- Dans Eclipse, aller dans Window/views/git repository. Dans la nouvelle fenetre sélectionner "cloner depuis un dépot distant" ou "créér un dépot local" 
- Pour cloner depuis le serveur local -> cloner depuis "http://alban@10.10.173.109/HelloWorld.git"
- A chaque modification, pour faire un commit et un push , dans Eclipse, aller dans Window/views/git stage et dans la nouvelle fenêtre on peut faire les push et les co
mmits.
- Pour faire des pull, dans la fenêtre git repository, faire clic droit et "pull".

![](/Users/Alban/Google Drive/Cours/METHODE_AGILE/agile-tp1/Capture d’écran 2015-11-18 à 17.03.07.png)
 


##Problemes rencontrés 
- Gitstack port deja utilisé -> changement de port en 8088
- Probleme de pare feu sur le serveur -> désactivé

##Gestion des conflits
- Conflits lors de deux modifications du même fichier au meme endroit 
![](/Users/Alban/Google Drive/Cours/METHODE_AGILE/agile-tp1/Capture d’écran - copie.png)
![](/Users/Alban/Google Drive/Cours/METHODE_AGILE/agile-tp1/Capture d’écran.png)
-> résolus avec le gestionnaire de conflits qui s'affiche 
![](/Users/Alban/Google Drive/Cours/METHODE_AGILE/agile-tp1/Capture d’écran 1.png)